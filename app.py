import json
from flask import Flask,request,jsonify
from flask_cors import CORS


app = Flask(__name__)
CORS(app)

@app.route("/suma/<n1>/<n2>",methods=['POST'])
def suma(n1,n2):
    resultado = int(n1) + int(n2)
    return "Resultado: "+str(resultado)


@app.route("/resta/<n1>/<n2>",methods=['POST'])
def resta(n1,n2):
    
    resultado = int(n1) - int(n2)
    return "Resultado: "+str(resultado)


@app.route('/')
def main():
     return """
    <h1>0:45</h1>

    <iframe src="https://www.youtube.com/embed/dQw4w9WgXcQ" width="853" height="480" frameborder="0" allowfullscreen></iframe>
    """


if __name__ == '__main__':
    
    app.run(port=3000, debug=True)